<?php

use Models\OnCallEntry;
use Models\OnCallLocation;
use Models\OnCallTechnician;
use \App;

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		// get todays date so the system knows which entrys to display
		$todaysDate = date('Y-m-d');
		// get the current period of the day (AM/PM) so the sidebar knows which technician is currently on call
		$onCallPeriod = date('A');
		// providers for all models
		$onCallEntrysProvider = App::make("Services\Repository\OnCallEntrysProvider");
		$currentOnCallEntrysProvider = App::make("Services\Repository\CurrentOnCallEntrysProvider");
		$onCallLocationsProvider = App::make("Services\Repository\OnCallLocationsProvider");

		return View::make('new',
			array(
				'locations'=>$onCallLocationsProvider->getLocations(),
				'entrys'=>$onCallEntrysProvider->getEntrys($todaysDate),
				'current'=>$currentOnCallEntrysProvider->getCurrentEntrys($todaysDate, $onCallPeriod)
			)
		);
	}

	public function localview($loc)
	{
		return View::make('location/'.$loc);
	}

}
