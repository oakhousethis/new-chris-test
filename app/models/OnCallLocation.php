<?php

namespace Models;

class OnCallLocation extends \Eloquent
{
    protected $table = 'onCallLocation';
    public $timestamps = false;
}