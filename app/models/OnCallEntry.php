<?php

namespace Models;

class OnCallEntry extends \Eloquent
{
    protected $table = 'onCallEntry';
    public $timestamps = false;

    public function location()
    {
        return $this->belongsTo('Models\onCallLocation','onCallLocation');
    }

    public function technician()
    {
        return $this->belongsTo('Models\onCallTechnician','techID');
    }

}