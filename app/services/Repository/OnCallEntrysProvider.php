<?php

namespace Services\Repository;

use Models\OnCallEntry;
use \App;

class OnCallEntrysProvider
{
	public function getEntrys($todaysDate) {
		// gets the location and technician information
		$getLocationByEntryId = App::make("Services\Repository\GetLocationByEntryId");
		$getTechnicianByEntryId = App::make("Services\Repository\GetTechnicianByEntryId");
		// gets all the entries that are set to todays date 
		$entrys = OnCallEntry::where('onCallDate', '=', $todaysDate)
		->orderBy('onCallPeriod', 'asc')
		->get();
		// for each entry change the techId from an id to and array of the technician's information
		// for each entry change the onCallLocation from an id to and array of the Location's information
		foreach ($entrys as $entry) {
			$entry->onCallLocation = $getLocationByEntryId->getLocationById($entry->onCallLocation);
			$entry->techID = $getTechnicianByEntryId->getTechnicianById($entry->techID);
		}

		return $entrys;

	}

}
