<?php

namespace Services\Repository;

use Models\OnCallLocation;

class OnCallLocationsProvider
{
	public function getLocations() {
		
		return OnCallLocation::all();

	}

}