<?php

namespace Services\Repository;

use Models\OnCallLocation;

class GetLocationByEntryId
{
	public function getLocationById($locationId) {
		
		return OnCallLocation::find($locationId);
	
	}

}
