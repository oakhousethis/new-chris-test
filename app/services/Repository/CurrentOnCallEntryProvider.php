<?php

namespace Services\Repository;

use Models\OnCallEntry;
use \App;

class CurrentOnCallEntrysProvider
{
	public function getCurrentEntrys($todaysDate,$onCallPeriod) {
		// checks if the onCallPeriod is AM or PM and converts it to a 1 for PM or a 0 for AM
		if ($onCallPeriod == "AM"){
			$onCallPeriod = 0;
		}
		else {
			$onCallPeriod = 1;
		}
		// gets the location and technician information
		$getLocationByEntryId = App::make("Services\Repository\GetLocationByEntryId");
		$getTechnicianByEntryId = App::make("Services\Repository\GetTechnicianByEntryId");
		// gets all the entries that are set to todays date and current time period (AM or PM)
		$entrys = OnCallEntry::where('onCallDate', '=', $todaysDate)
		->where('onCallPeriod', '=', '$onCallPeriod')
		->orderBy('onCallPeriod', 'asc')
		->get();
		// for each entry change the techId from an id to and array of the technician's information
		// for each entry change the onCallLocation from an id to and array of the Location's information
		foreach ($entrys as $entry) {
			$entry->onCallLocation = $getLocationByEntryId->getLocationById($entry->onCallLocation);
			$entry->techID = $getTechnicianByEntryId->getTechnicianById($entry->techID);
		}

		return $entrys;

	}

}
