<?php

namespace Services\Repository;

use Models\OnCallTechnician;

class GetTechnicianByEntryId
{
	public function getTechnicianById($techId) {
		
		return OnCallTechnician::find($techId);

	}

}