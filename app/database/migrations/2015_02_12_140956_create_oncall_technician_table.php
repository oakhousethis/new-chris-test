<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOncallTechnicianTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('onCallTechnician');
		Schema::create('onCallTechnician',function($table){
			$table->engine='InnoDB';
			$table->increments('id');
			$table->string('technicianName');
			$table->string('technicianEmail');
			$table->string('technicianPhone');
			$table->string('technicianPhoneAlt');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('onCallTechnician');
	}

}
