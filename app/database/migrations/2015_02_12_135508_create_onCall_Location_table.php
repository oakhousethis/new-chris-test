<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOncallLocationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('onCallLocation');
		Schema::create('onCallLocation',function($table){
			$table->engine='InnoDB';
			$table->increments('id');
			$table->string('locationName');
			$table->string('locationCode');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('onCallLocation');
	}

}
