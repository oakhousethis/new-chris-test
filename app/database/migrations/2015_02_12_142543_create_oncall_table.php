<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOncallTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('onCallEntry');
		Schema::create('onCallEntry',function($table){
			$table->engine='InnoDB';
			$table->increments('id');
			$table->integer('techID')->unsigned();
			$table->foreign('techID')->references('id')->on('onCallTechnician');
			$table->integer('onCallLocation')->unsigned();
			$table->foreign('onCallLocation')->references('id')->on('onCallLocation');
			$table->date('onCallDate');
			$table->boolean('onCallPeriod');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('onCallEntry');
	}

}
