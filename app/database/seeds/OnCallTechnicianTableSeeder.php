<?php

use Models\OnCallTechnician;


class OnCallTechnicianTableSeeder extends Seeder {
	public function run()
	{
		$OnCallTechnicians=array(
			array(
				
				'technicianName'=>'Chris Richards',
				'technicianEmail'=>'webteam@this.nhs.uk',
				'technicianPhone'=>'0771234567',
				'technicianPhoneAlt'=>'01484 355666'
			),
			array(
				'technicianName'=>'Tom Amos',
				'technicianEmail'=>'webteam@this.nhs.uk',
				'technicianPhone'=>'0771234567',
				'technicianPhoneAlt'=>'01484 355666'
			),
			array(
				'technicianName'=>'Jason Cresswell',
				'technicianEmail'=>'webteam@this.nhs.uk',
				'technicianPhone'=>'0771234567',
				'technicianPhoneAlt'=>'01484 355666'
			)
		);
		foreach ($OnCallTechnicians as $OnCallTechnician) {
            OnCallTechnician::create(
                array(
                    'technicianName' => $OnCallTechnician['technicianName'],
                    'technicianEmail'=>$OnCallTechnician['technicianEmail'],
                    'technicianPhone' => $OnCallTechnician['technicianPhone'],
                    'technicianPhoneAlt'=>$OnCallTechnician['technicianPhoneAlt']
                )
        	);
        }
	}
}