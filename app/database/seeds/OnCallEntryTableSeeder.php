<?php

use Models\OnCallEntry;

class OnCallEntryTableSeeder extends Seeder {
	public function run()
	{
		$onCallEntrys=array(
			array(
				'techID'=>1,
				'onCallLocation'=>1,
				'onCallDate'=>'2015-02-16',
				'onCallPeriod'=>1
			),
			array(
				'techID'=>1,
				'onCallLocation'=>1,
				'onCallDate'=>'2015-02-17',
				'onCallPeriod'=>0
			),
			array(
				'techID'=>2,
				'onCallLocation'=>2,
				'onCallDate'=>'2015-02-16',
				'onCallPeriod'=>1
				),
			array(
				'techID'=>2,
				'onCallLocation'=>2,
				'onCallDate'=>'2015-02-17',
				'onCallPeriod'=>0
				),
			array(
				'techID'=>3,
				'onCallLocation'=>3,
				'onCallDate'=>'2015-02-16',
				'onCallPeriod'=>1
				),
			array(
				'techID'=>3,
				'onCallLocation'=>3,
				'onCallDate'=>'2015-02-17',
				'onCallPeriod'=>0
			)
		);
		foreach ($onCallEntrys as $onCallEntry) {
            OnCallEntry::create(
                array(
                    'techID' => $onCallEntry['techID'],
                    'onCallLocation'=>$onCallEntry['onCallLocation'],
                    'onCallDate' => $onCallEntry['onCallDate'],
                    'onCallPeriod'=>$onCallEntry['onCallPeriod']
                )
        	);
        }
	}
}