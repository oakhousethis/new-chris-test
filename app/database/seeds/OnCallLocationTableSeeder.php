<?php

use Models\OnCallLocation;

class OnCallLocationTableSeeder extends Seeder {
	public function run()
	{
		$onCallLocations=array(
			array(
				'locationName'=>'Huddersfield Royal Infirmary',
				'locationCode'=>'HRI'
			),
			array(
				'locationName'=>'Calderdale Royal Hospital',
				'locationCode'=>'CRH'
			),
			array(
				'locationName'=>'Wakefield District',
				'locationCode'=>'WAKE'
				)
			);
		foreach ($onCallLocations as $onCallLocation) {
            OnCallLocation::create(
                array(
                    'locationName' => $onCallLocation['locationName'],
                    'locationCode'=>$onCallLocation['locationCode']
                )
        	);
        }
	}
}