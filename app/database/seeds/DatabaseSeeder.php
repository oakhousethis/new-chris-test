<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('OnCallLocationTableSeeder');
		$this->call('OnCallTechnicianTableSeeder');
		$this->call('OnCallEntryTableSeeder');
	}

}
