<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get(
	'/',
    'HomeController@showWelcome'
);
Route::get(
	//when the user clicks on a view link send them to /location
    '/location/{loc}',
    'HomeController@localview'
);